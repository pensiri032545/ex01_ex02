
import 'dart:io';

void main() {
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'vegetarian': 6.5,
  };

  List menu = const ['margherita', 'pepperoni', 'vegetarian'];
  List? order = [];
  var numMenu = 0, total = 0.0;

  print("Please select your menu.\n"
      " 1. margherita\n"
      " 2. pepperoni\n"
      " 3. vegetarian");
  print("How many menu do you want to order?");
  numMenu = int.parse(stdin.readLineSync()!);
  List<bool> checkMenu = List<bool>.filled(numMenu, false);
  if (numMenu<=pizzaPrices.length) {

    print("Order Menu?");
    for (int i=0; i<numMenu; i++) {
      order.add(stdin.readLineSync());
    }

    for (var i=0; i<numMenu; i++) {
      checkMenu[i] = false;
      for (var j=0; j<pizzaPrices.length; j++) {
        if (order[i].compareTo(menu[j]) == 0) {
          checkMenu[i] = true;
          total += pizzaPrices[menu[j]]!;
        }
      }
    }

    for (var check=0; check<checkMenu.length; check++) {
      if (checkMenu[check] == false) {
        print("${order[check]} pizza is not on the menu.");
      }
    }
  }
  print("Total : \$$total");
}
//6450110008