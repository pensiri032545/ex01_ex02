
import 'dart:io';

void main() {
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'vegetarian': 6.5, };
  const order = ['margherita', 'pepperoni', 'pineapple'];

  double calPrice(Map pizzaPrices, List order) {
    var total = 0.0;
    for (var item in order) {
      final price = pizzaPrices[item];
      if (price != null) {
        total += price;
      }
    }
    return total;
  }
  double total = calPrice(pizzaPrices, order);
  print("Pizza Prices : $pizzaPrices");
  print("order : $order");
  print('Total: \$$total');
}
//6450110008