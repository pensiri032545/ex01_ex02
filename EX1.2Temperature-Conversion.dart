
import 'dart:io';

void main() {
  // Fahrenheit to Celsius:   (°F − 32) / 1.8 = °C
  double tempFarenheit;

  print("Enter your temperature farenheit : ");
  tempFarenheit = double.parse(stdin.readLineSync()!);
  print("${tempFarenheit.toInt()}F = ${(tempFarenheit - 32) / 1.8}C");
}
//6450110008
